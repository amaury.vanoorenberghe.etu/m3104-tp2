@ECHO OFF

PUSHD %~dp0

MKDIR build > NUL

javac -d build/ src/*.java

POPD