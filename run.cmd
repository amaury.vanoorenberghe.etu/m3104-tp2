@ECHO OFF
SETLOCAL enabledelayedexpansion enableextensions

PUSHD %~dp0

SET LIBS=
FOR %%F IN (%CD%\lib\*.jar) DO SET LIBS=!LIBS!;%%F
SET LIBS=%LIBS:~1%

SET CLASSES=
FOR %%F IN (%CD%\build\*.class) DO SET CLASSES=!CLASSES!;%%F
SET CLASSES=%CLASSES:~1%

SET CLASSPATH=%CD%\build;%CD%\lib;%LIBS%;%CLASSES%

java "%1"

POPD

ENDLOCAL