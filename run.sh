#!/bin/bash

LIBS=$(ls -R lib/*.jar | tr "\n" ":").

java -cp "${LIBS}":./build "$1"