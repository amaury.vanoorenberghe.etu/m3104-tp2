import java.sql.*;
import java.util.*;

public class Compter
{
    // AVANT LA QUESTION SUR LES .PROPERTIES
    // public static final String DRIVER = "org.postgresql.Driver";
    // public static final String URL = "jdbc:postgresql://psqlserv:5432/fi2";
    // public static final String LOGIN = "amauryvanoorenbergheetu";
    // public static final String PASSWORD = "moi";

    public static void main(String args[]) throws Exception
    {      
        // AVANT LA QUESTION SUR LES .PROPERTIES
        // enregistrement du driver
        // Class.forName(DRIVER);
      
        // connexion à la base
        // Connection con = DriverManager.getConnection(URL, LOGIN, PASSWORD);

        // APRÈS LA QUESTION SUR LES .PROPERTIES
        DataSource source = new DataSource();
        Connection con = source.getConnection();

        try {
           run(con);
        } catch (SQLException sqlExc) {
            System.err.println(sqlExc.getMessage());
            sqlExc.printStackTrace();
        } catch (Exception anyExc) {
            anyExc.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception closeExc) {
                closeExc.printStackTrace();
            }
        }

        System.out.println("Ok.");
    }

    public static void run(Connection con) throws SQLException {
        // Compter les lignes de la table clients
		String dataQuery = "SELECT COUNT(*) FROM clients";
		PreparedStatement dataStatement = con.prepareStatement(dataQuery);
        ResultSet data = dataStatement.executeQuery();
		
		if (data.next()) {
			int count = data.getInt(1);			
			
			System.out.println(String.format("La table clients comporte %d lignes", count));
		}
    }
}