import java.sql.*;

public class Create 
{
    // AVANT LA QUESTION SUR LES .PROPERTIES
    // public static final String DRIVER = "org.postgresql.Driver";
    // public static final String URL = "jdbc:postgresql://psqlserv:5432/fi2";
    // public static final String LOGIN = "amauryvanoorenbergheetu";
    // public static final String PASSWORD = "moi";

    public static void main(String args[]) throws Exception
    {
        // AVANT LA QUESTION SUR LES .PROPERTIES
        // enregistrement du driver
        // Class.forName(DRIVER);
      
        // connexion à la base
        // Connection con = DriverManager.getConnection(URL, LOGIN, PASSWORD);

        // APRÈS LA QUESTION SUR LES .PROPERTIES
        DataSource source = new DataSource();
        Connection con = source.getConnection();

        try {
           run(con);
        } catch (SQLException sqlExc) {
            System.err.println(sqlExc.getMessage());
            sqlExc.printStackTrace();
        } catch (Exception anyExc) {
            anyExc.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception closeExc) {
                closeExc.printStackTrace();
            }
        }

        System.out.println("Ok.");
    }

    public static void run(Connection con) throws SQLException {
        // Destruction de la table si elle existe
        String dropQuery = "DROP TABLE IF EXISTS clients";
        PreparedStatement dropStatement = con.prepareStatement(dropQuery);
        dropStatement.executeUpdate();

        // Execution de la requete de création de table
        String createQuery = "CREATE TABLE clients (id_client SERIAL, nom varchar(10), prenom varchar(10), age int, CONSTRAINT pk_client PRIMARY KEY (id_client))";
        PreparedStatement createStatement = con.prepareStatement(createQuery);
        createStatement.executeUpdate();
    }
}
