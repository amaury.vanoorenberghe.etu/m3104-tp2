import java.util.*;
import java.io.*;
import java.sql.*;

public class DataSource {
    private static final Set<String> LOADED_DRIVERS = new HashSet<String>();

    private Connection connection;
    private final String DRIVER, URL, USER, PASSWORD;

	public DataSource() {
		Properties general = loadProperties("config.properties");
		
        String credentialsFile = general.getProperty("credentials");

        Properties credentials = loadProperties(String.format("credentials/%s.properties", credentialsFile));

        DRIVER = credentials.getProperty("driver");
        URL = credentials.getProperty("url");
        USER = credentials.getProperty("user");
        PASSWORD = credentials.getProperty("password", "");
	}
	
	private Properties loadProperties(String fileName) {
		Properties result = new Properties();
		
		try (InputStream stream = new FileInputStream(String.format("config/%s", fileName))) {
			result.load(stream);
		} catch (Exception any) {
			any.printStackTrace();
		}
        
		return result;		
	}

    public Connection getConnection() throws SQLException {
        loadDriver(DRIVER);

        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        }

        return connection;
    }

    private static void loadDriver(String name) {
        if (!LOADED_DRIVERS.contains(name)) {
            try {
                Class.forName(name);
                LOADED_DRIVERS.add(name);
            } catch (ClassNotFoundException cnfe) {
                System.err.println(String.format("Impossible de charger le type %s", name));
                cnfe.printStackTrace();
            }
        }
    }
}