import java.sql.*;
import java.util.*;

public class Insert 
{
    // AVANT LA QUESTION SUR LES .PROPERTIES
    // public static final String DRIVER = "org.postgresql.Driver";
    // public static final String URL = "jdbc:postgresql://psqlserv:5432/fi2";
    // public static final String LOGIN = "amauryvanoorenbergheetu";
    // public static final String PASSWORD = "moi";

    public static void main(String args[]) throws Exception
    {      
        // AVANT LA QUESTION SUR LES .PROPERTIES
        // enregistrement du driver
        // Class.forName(DRIVER);
      
        // connexion à la base
        // Connection con = DriverManager.getConnection(URL, LOGIN, PASSWORD);

        // APRÈS LA QUESTION SUR LES .PROPERTIES
        DataSource source = new DataSource();
        Connection con = source.getConnection();

        try {
           run(con);
        } catch (SQLException sqlExc) {
            System.err.println(sqlExc.getMessage());
            sqlExc.printStackTrace();
        } catch (Exception anyExc) {
            anyExc.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception closeExc) {
                closeExc.printStackTrace();
            }
        }

        System.out.println("Ok.");
    }

    public static void run(Connection con) throws SQLException {
        // Vider la table avant d'insérer de nouveaux clients
        String removeQuery = "DELETE FROM clients";
        PreparedStatement removeStatement = con.prepareStatement(removeQuery);
        removeStatement.executeUpdate();
		
		Random RNG = new Random();
		
		for (int i = 0; i < 1000; ++i) {
			// Ajout du client numéro i
			
			String clientQuery = "INSERT INTO clients (nom, prenom, age) VALUES (?, ?, ?)";
			
			PreparedStatement clientStatement = con.prepareStatement(clientQuery);
			
			clientStatement.setString	(1, "nom_" + i);
			clientStatement.setString	(2, "prenom_" + i);
			clientStatement.setInt		(3, RNG.nextInt(50) + 20);
			
			clientStatement.executeUpdate();
		}
    }
}
